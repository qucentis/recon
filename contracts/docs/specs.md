# Specs

- `Recon` is a master contract that can generate child contracts
- Many variants of master contracts are possible that optimizes different aspects
- The variant we are building is `FlatReduction`

## FlatReduction

- `Recon` contract that can generate `flatReduction` contract with consumers
- It takes in one variable: `reductionPercentage`
- Mines a contract thats presented for users to invoke (or buy)

### User flow

- A user chooses to invoke a `flatReduction` contract
- Its takes two variables: `accountNumber`, `movingAvergaeOverThreeYears`, & `attemptCount`
- The contracts then waits to get resolved at the end of the billing cycle
- If the consumer was able to meet the limit he set for himself he will get the amount that was contractually agreed
- If he fails to meet it he will loose the money spent for the contract
- Contract is supposed to be manually triggered after the bills have been sent & APIs have been updated
- There should only be a single contract per `accountNumber`
- Contract should save the contract data in data field for easy parsing

### Challenges

- Contracts will become difficult to acheive as `movingAverage` changes. So may be we will calculate it on an yearly basis.
