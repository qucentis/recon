import jwt from "jsonwebtoken"
import sigUtil from "eth-sig-util"

let secret = process.env.SECRET || "secret"
let greeting = "open sesame"

export function authenticate(req, res) {
  const recovered = sigUtil.recoverTypedSignature({
    data: [
      {
        type: "string",
        name: "Message",
        value: greeting,
      },
    ],
    sig: req.body.sig,
  })

  if (recovered === req.body.owner) {
    let payload = {
      user: req.body.owner,
    }

    let token = jwt.sign(payload, secret, {
      expiresIn: "1d",
    })

    res.json({ success: true, token: token, user: req.body.owner })
  } else {
    res.json({ error: "failed to verify signer" })
  }
}

export function verifyJWT(req, res, next) {
  var authHeader = req.headers["authorization"]

  if (authHeader) {
    let bearerToken = authHeader.split(" ")

    if (bearerToken.length == 2) {
      jwt.verify(bearerToken[1], secret, function(error, data) {
        if (error) {
          return res
            .status(401)
            .send({ success: false, error: "Invalid authorization token" })
        }
        req.data = data
        req.user = data.user
        next()
      })
    }
  } else {
    res
      .status(401)
      .send({ success: false, error: "An authorization header is required" })
  }
}
