import fs from "fs"
import { Router } from "express"
import solc from "solc"

import global from "global"
import store from "store"
import { authenticate, verifyJWT } from "auth"

function isAuthority(req, res, next) {
  if (req.user !== global.authority) {
    return res
      .status(401)
      .send({ success: false, error: "Not a registered authority!" })
  }

  next()
}

function isNotAuthority(req, res, next) {
  if (req.user === global.authority) {
    return res
      .status(401)
      .send({ success: false, error: "Can't be a registered authority!" })
  }

  next()
}

export default () => {
  let api = Router()

  api.get("/", (req, res) => {
    res.json({ status: "ok" })
  })

  api.get("/authority", (req, res) => {
    res.json({ authority: global.authority })
  })

  api.post("/authenticate", authenticate)

  api.get("/protected", verifyJWT, (req, res) => {
    res.json({ user: req.user })
  })

  api.post("/compile", verifyJWT, (req, res) => {
    var result = solc.compile({ sources: req.body.input }, 1)
    res.json(result)
  })

  api.post("/add-contract", verifyJWT, isAuthority, (req, res) => {
    store
      .update("contracts", contracts => {
        if (!contracts) contracts = []
        //contracts = []
        contracts.push({
          name: req.body.name,
          address: req.body.address,
          timestamp: req.body.timestamp,
        })
        return contracts
      })
      .then(result => {
        return store.storage.setItem(req.body.address, req.body)
      })
      .then(result => {
        res.json({ success: true })
      })
  })

  api.get("/deployed-contracts", verifyJWT, (req, res) => {
    store.storage.getItem("contracts").then(contracts => {
      res.json(contracts)
    })
  })

  api.get("/deployed-contract/:address", verifyJWT, (req, res) => {
    store.storage.getItem(req.params.address).then(contract => {
      res.json(contract)
    })
  })

  api.get("/templates", verifyJWT, isAuthority, (req, res) => {
    res.json([
      {
        name: "Percentage Reduction",
        source: fs.readFileSync("../contracts/recon_contracts.sol", "utf8"),
        contractName: "UserContractFactory",
      },
    ])
  })

  return api
}
