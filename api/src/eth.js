import Eth from "ethjs"

let network = process.env.NETWORK || "localhost"

let url = ""

switch (network) {
  case "ropsten":
    url = "https://ropsten.infura.io"
    break

  default:
    url = "http://localhost:8545"
}

let eth = new Eth(new Eth.HttpProvider(url))

export default eth
