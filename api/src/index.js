import http from "http"
import express from "express"
import morgan from "morgan"
import bodyParser from "body-parser"

import global from "global"
import store from "store"
import eth from "eth"
import api from "api"

store.init()

eth.accounts().then(accounts => {
  global.authority = accounts[0]
})

let app = express()

app.server = http.createServer(app)

app.use(morgan("dev"))

app.use(bodyParser.json())

app.use("/api", api())

app.use(function(err, req, res, next) {
  console.error("ERROR", err)
  res.json({ error: err })
})

app.use(function(req, res, next) {
  try {
    res.json({ error: "not-found" })
  } catch (err) {
    res.json({ error: "server-error" })
  }
})

app.server.listen(process.env.PORT || 5000)

console.log(`Started on port ${app.server.address().port}`)
