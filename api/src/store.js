import storage from "node-persist"

export function init() {
  return storage
    .init({
      dir: "data",
    })
    .then(result => {
      console.log("storage: ", result.dir)
      return result.dir
    })
}

export function update(key, transform) {
  let result = null

  return storage
    .getItem(key)
    .then(value => {
      result = transform(value)
      return storage.setItem(key, result)
    })
    .then(_ => {
      return result
    })
    .catch(err => {
      throw err
    })
}

export default {
  storage,
  init,
  update,
}
