require 'pdf-reader'
require 'json'

file = ARGV[0]

fail 'No file specified' unless file

reader = PDF::Reader.new(file)

raw_content = reader.pages.map { |p| p.raw_content }.join("\n")

bill_data = raw_content.scan(/\(.*?\)Tj/)[0..1]

def extract(format)
  format.match(/(\.|\d)+/)[0]
end

cleaned_data = bill_data.map { |d| extract(d) }

bill = {
  accountNumber: cleaned_data[0],
  amount: cleaned_data[1]
}

puts JSON::dump bill
