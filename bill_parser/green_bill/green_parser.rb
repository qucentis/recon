require 'pdf-reader'
require 'json'

file = ARGV[0]

fail 'No file specified' unless file

reader = PDF::Reader.new(file)

page_1 = reader.pages[1].text

def extract_electricity(page)
  page.scan(/Kilowatt Hours.*?: \d+.*?(\d+).*?Carbon/m).flatten[0]
end

def extract_account_number(page)
  page.match(/Account Number.*?(?<an>\d{10}).*?Meternu/m)[:an]
end

bill = {
  accountNumber: extract_account_number(page_1),
  amount: extract_electricity(page_1)
}

puts JSON::dump(bill)
