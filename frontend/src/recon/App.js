import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import API from "recon/api"
import { authenticate } from "recon/api/functions"

import SiteWrapper from "core/SiteWrapper"
import { DashboardPage } from "./authority/DashboardPage"
import { CreatePage } from "./authority/CreatePage"
import { ListPage } from "./authority/ListPage"
import { ContractPage } from "./contracts/ContractPage"
import { NotFound } from "./common/NotFound"

window.api = API

authenticate()

class App extends React.Component {
  render() {
    return (
      <SiteWrapper>
        <Router>
          <Switch>
            <Route exact path="/" component={DashboardPage} />
            <Route exact path="/authority" component={DashboardPage} />
            <Route exact path="/authority/contracts/" component={ListPage} />
            <Route
              exact
              path="/authority/contract-templates/:id/create"
              component={CreatePage}
            />
            <Route
              exact
              path="/contracts/:contractAddress"
              component={ContractPage}
            />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </SiteWrapper>
    )
  }
}

export default App
