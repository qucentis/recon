import React from "react"
import { NavLink } from "react-router-dom"

export const contracts = {
  dataSource: [
    {
      key: "3",
      name: "Peak Reduction",
      address: "0x744d70FDBE2Ba4CF95131626614a1763DF805B9E",
      timestamp: 1512487487,
    },
    {
      key: "4",
      name: "Percentage Reduction",
      address: "0xcC361a1C660147B2b452121056574011A9817474",
      timestamp: 1512487487,
    },
  ],
  columns: [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Address",
      dataIndex: "address",
      render: address => (
        <NavLink className="code b" to={`/contracts/${address}`}>
          {address}
        </NavLink>
      ),
    },
    {
      title: "Date of Deployment",
      dataIndex: "timestamp",
    },
  ],
}
