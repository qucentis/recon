const instance = `
# Recon Consumer Contract

The contract is a legal binding between the invoker identified by the public key that invoked the contract & Dubai Energy & Water Authority (Authority from now on). The invoker agrees to the following:

- All the terms have been read & agreed.
- The resolution of contract is at the end of billing cycle
- All transactions will be in Ether
- The Energy Saving Certificate generated will be fully owned by the Authority

## Penalty Clause

- The amount staked for the contract will be sent to Authority if the contract resolves without meeting the target

## Note

- Please make sure you are careful in entering the account number, no data can be editted after submission.
- This is a sample contract & is working on the testnet
- DEWA isn't officially using Recon yet, the name is used as an example
`

const proposal = `
# Recon Authority Contract

The contract is a proposal using which anyone can invoke a contract between them & DEWA for an energy saving target.

- All the terms have been read & agreed.
- The resolution of contract is at the end of billing cycle
- All transactions will be in Ether
- The Energy Saving Certificate generated will be fully owned by the Authority

## Penalty Clause

- The amount staked for the contract will be sent to Authority if the contract resolves without meeting the target

## Note

- Please make sure you are careful in entering the account number, no data can be editted after submission.
- This is a sample contract & is working on the testnet
- DEWA isn't officially using Recon yet, the name is used as an example
`

export const terms = {
  instance,
  proposal,
}
