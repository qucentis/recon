export function sampleSource(props) {
  const name = props.name || "CONTRACT_NAME"
  const reduction = props.reduction || "REDUCTION_VALUE"
  const description = props.description || "DESCRIPTION"

  return `pragma solidity ^0.4.0;

contract UserContract {
  uint percentageReduction = ${reduction};

  address userAddress;
  address authorityAddress;
  string userAccount;
  uint averageUnitsConsumed;

  bool resolved = false;
  bool escrowedAuthority = false;
  bool escrowedUser = false;
  uint minimumEscrowAmount = 0;
  uint targetUsage = 0;
  bool active = false;
  bool targetAchieved = false;

  uint totalAmountEscrowed = 0;
  uint userEscrowAmount = 0;
  uint authorityEscrowAmount = 0;

  uint unitsConsumed = 0;

  function UserContract(string _userAccount, address _userAddress,
                        uint _averageUnitsConsumed,
                        address _authorityAddress) public {

                          userAccount = _userAccount;
                          userAddress = _userAddress;
                          authorityAddress = _authorityAddress;
                          averageUnitsConsumed = _averageUnitsConsumed;
                          targetUsage = averageUnitsConsumed - averageUnitsConsumed * percentageReduction / 100;
                          minimumEscrowAmount = calculateMinimumEscrowAmount(averageUnitsConsumed, targetUsage);
  }



  function calculateMinimumEscrowAmount(uint _averageUnitsConsumed, uint _targetUsage) private pure returns (uint) {
    return (_averageUnitsConsumed - _targetUsage) / 10;
  }

  function () public payable {
    if (msg.sender == userAddress) {
      userEscrowAmount += msg.value;
      if (userEscrowAmount >= minimumEscrowAmount) {
        escrowedUser = true;
      }
    } else if (msg.sender == authorityAddress) {
      authorityEscrowAmount += msg.value;
      if (authorityEscrowAmount >= minimumEscrowAmount) {
        escrowedAuthority = true;
      }
    }
    if (escrowedUser && escrowedAuthority) {
      active = true;
    }
    totalAmountEscrowed += msg.value;
  }

  function resolve(uint _unitsConsumed) public returns (bool) {
    require(active);
    require(!resolved);
    unitsConsumed = _unitsConsumed;

    if (targetUsage < unitsConsumed) {
      if (!authorityAddress.send(totalAmountEscrowed)) {
        revert();
        targetAchieved = false;
      }
    } else {
      if (!userAddress.send(totalAmountEscrowed)) {
        revert();
        targetAchieved = true;
      }
    }
    resolved = true;
    active = false;
    return true;
  }

  function isActive() public view returns (bool) {
    return active;
  }

  function isResolved() public view returns (bool) {
    return resolved;
  }

  function isEscrowUser() public view returns (bool) {
    return escrowedUser;
  }
  function isEscrowAuthority() public view returns (bool) {
    return escrowedAuthority;
  }

  function isTargetAchieved() public view returns (bool) {
    return targetAchieved;
  }

  function getTokenResource() public view returns (uint) {
    return averageUnitsConsumed - unitsConsumed;
  }

  function getTargetUsage() public view returns (uint) {
    return targetUsage;
  }
}

contract UserContractFactory {

  address authorityAddress;

  mapping (address => address) contracts;

  event UserContractDeployed(address contractAddress);
  event UserContractResolved(address contractAddress);


  function UserContractFactory(address _authorityAddress) public {
    authorityAddress = _authorityAddress;
  }


  function deploy(string _userAccount,
                        uint _averageUnitsConsumed
                        ) public returns (address) {
                          if (contracts[msg.sender] == 0x0) {
                            UserContract child = new UserContract(_userAccount,
                                                                  msg.sender,
                                                                _averageUnitsConsumed,
                                                                authorityAddress);

                           contracts[msg.sender] = child;
                           UserContractDeployed(child);
                           return child;
                          } else {
                            revert();
                          }
  }

  function resolve(address _userAddress, uint unitsConsumed) public {
    require(msg.sender == authorityAddress);
    UserContract child = UserContract(contracts[_userAddress]);
    if (child.resolve(unitsConsumed)) {
      UserContractResolved(contracts[_userAddress]);
      delete contracts[_userAddress];
    }
  }

  function getContractForUser(address _userAddress) public view returns (address){
    return contracts[_userAddress];
  }
}
`
}
