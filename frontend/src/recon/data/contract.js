import { terms } from "./terms"

export const proposal = {
  type: "proposal",
  payload: {
    name: "Percentage Reduction Proposal",
    displayPicture: "/logo-sq.png",
    coverImage: "/dubai.png",
    source: `
      function something() {
        return true
      }
    `,
    description: "Short text about contract",
    text: terms.proposal,
    issuer: {
      name: "Dubai Energy & Water Authority",
      image: "<id>",
    },
    duration: "Feb 1 - 28",
    percentageReduction: 2,
  },
  timestamp: 1512697382,
  address: "0xcC361a1C660147B2b452121056574011A9817474",
}

export const instance = {
  type: "instance",
  payload: {
    name: "Percentage Reduction Instance",
    displayPicture: "/logo-sq.png",
    coverImage: "/dubai.png",
    source: `function something() {
  return true
}

import React from "react"
import SyntaxHighlighter from "react-syntax-highlighter/prism"
import { atomDark } from "react-syntax-highlighter/styles/prism"

export function SourceCode({ contractData }) {
  const source = contractData.source

  return (
    <div className="bg-black pre code recon-contract-editor">
      <SyntaxHighlighter showLineNumbers language="javascript" style={atomDark}>
        {source}
      </SyntaxHighlighter>
    </div>
  )
}
    `,
    description: "Short text about contract",
    text: terms.instance,
    contractParty: {
      accountNumber: "234667",
    },
    status: {
      isResolved: true,
      isSuccess: true,
    },
    duration: "Jan 1 — 31",
    resolutionDate: 2558212475,
    percentageReduction: 3,
    issuer: {
      name: "DEWA",
      image: "<id>",
    },
  },
  timestamp: 1512487381,
  address: "0x744d70FDBE2Ba4CF95131626614a1763DF805B9E",
}
