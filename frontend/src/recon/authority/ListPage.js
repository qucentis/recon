import React from "react"
import { Table } from "antd"
import moment from "moment"

import NestedContainer from "core/NestedContainer"
import { SideBar } from "recon/common/SideBar"
import { Header } from "recon/common/Header"
import { ContentContainer } from "recon/common/ContentContainer"
import { generateRouteMethods } from "recon/utils"

import { contracts } from "recon/data/contracts"

export class ListPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = { contracts }

    // Disabling as the code wasn't working at the time of demo
    // window.api.deployedContracts().then(result => {
    //   contracts.dataSource = result.map((c, i) => {
    //     c.key = i
    //     c.timestamp = moment(c.timestamp).format("DD MMM YYYY")
    //     return c
    //   })
    //
    //   this.setState({ contracts })
    // })
  }

  render() {
    let routeMethods = generateRouteMethods(this.props)

    return (
      <div className="vh-100 flex flex-column">
        <Header />
        <NestedContainer className="ch flex-auto" innerClassName="bg-black-10">
          <div className="flex h-100">
            <SideBar
              selectedKey="routeToContracts"
              routeMethods={routeMethods}
            />
            <ContentContainer>
              <Table {...this.state.contracts} />
            </ContentContainer>
          </div>
        </NestedContainer>
      </div>
    )
  }
}
