import React from "react"

import NestedContainer from "core/NestedContainer"
import { SideBar } from "recon/common/SideBar"
import { Header } from "recon/common/Header"
import { ContentContainer } from "recon/common/ContentContainer"

export function DetailPage() {
  return (
    <div className="vh-100 flex flex-column">
      <Header />
      <NestedContainer className="ch flex-auto" innerClassName="bg-black-10">
        <div className="flex h-100">
          <SideBar selectedKey="5" />
          <ContentContainer />
        </div>
      </NestedContainer>
    </div>
  )
}
