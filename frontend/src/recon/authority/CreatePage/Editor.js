import React from "react"
import SyntaxHighlighter from "react-syntax-highlighter/prism"
import { atomDark } from "react-syntax-highlighter/styles/prism"

export function Editor(props) {
  const { sampleSource } = props

  return (
    <div className="bg-black pre code recon-contract-editor">
      <SyntaxHighlighter showLineNumbers language="javascript" style={atomDark}>
        {sampleSource}
      </SyntaxHighlighter>
    </div>
  )
}
