import React from "react"

import NestedContainer from "core/NestedContainer"
import { SideBar } from "recon/common/SideBar"
import { Header } from "recon/common/Header"
import { Creator } from "./Creator"
import { generateRouteMethods } from "recon/utils"

export function CreatePage(props) {
  const routeMethods = generateRouteMethods(props)

  return (
    <div className="vh-100 flex flex-column">
      <Header />
      <NestedContainer className="ch flex-auto" innerClassName="bg-black-10">
        <div className="flex h-100">
          <SideBar
            selectedKey="routeToContractCreate"
            routeMethods={routeMethods}
          />
          <Creator />
        </div>
      </NestedContainer>
    </div>
  )
}
