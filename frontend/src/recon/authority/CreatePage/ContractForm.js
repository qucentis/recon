import React from "react"
import { withRouter } from "react-router-dom"
import { Input, Button } from "antd"
import { Form, Field } from "react-final-form"

import { testDeployUserFactory } from "recon/api/test"

const { TextArea } = Input

class ContractForm extends React.Component {
  constructor(props) {
    super(props)

    console.log(props)

    this.state = {
      loading: false,
      text: "Deploy",
    }
  }

  deployContract = () => {
    if (this.state.loading) return

    this.setState({ loading: true, text: "Deploying" })

    testDeployUserFactory(address => {
      this.props.history.push(`/contracts/${address}`)
    })
  }

  render() {
    const { onSubmit, onChange } = this.props

    return (
      <div className="bg-black-20 pa3" style={{ width: "25rem" }}>
        <Form
          onSubmit={onSubmit}
          validate={onChange}
          render={({ handleSubmit, pristine, invalid }) => (
            <form onSubmit={handleSubmit}>
              <Field
                name="name"
                render={({ input, meta }) => (
                  <div className="flex flex-column mb2">
                    <label className="">Name</label>
                    <Input {...input} />
                    {meta.touched &&
                      meta.error && <span className="red">{meta.error}</span>}
                  </div>
                )}
              />
              <Field
                name="reduction"
                render={({ input, meta }) => (
                  <div className="flex flex-column mb2">
                    <label className="">Reduction</label>
                    <Input {...input} />
                    {meta.touched &&
                      meta.error && <span className="red">{meta.error}</span>}
                  </div>
                )}
              />
              <Field
                name="description"
                render={({ input, meta }) => (
                  <div className="flex flex-column mb2">
                    <label className="">Description</label>
                    <TextArea rows={8} {...input} />
                    {meta.touched &&
                      meta.error && <span className="red">{meta.error}</span>}
                  </div>
                )}
              />
              <Button
                loading={this.state.loading}
                type="primary"
                size="large"
                style={{ width: "100%" }}
                onClick={this.deployContract}
              >
                {this.state.text}
              </Button>
            </form>
          )}
        />
      </div>
    )
  }
}

ContractForm = withRouter(ContractForm)

export { ContractForm }
