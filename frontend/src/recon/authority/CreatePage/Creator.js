import React from "react"

import { Editor } from "./Editor"
import { ContractForm } from "./ContractForm"
import { sampleSource } from "recon/data/source"

export class Creator extends React.Component {
  state = {
    form: {
      name: "Amal Shehu",
      reduction: 15,
      description: "2 days after Christmas!",
    },
  }

  onSubmit = stuff => console.log("onSubmit", stuff)

  onChange = stuff => {
    this.setState(s => ({ form: stuff }))
  }

  render() {
    return (
      <div className="flex flex-auto pa3">
        <ContractForm onSubmit={this.onSubmit} onChange={this.onChange} />
        <Editor sampleSource={sampleSource(this.state.form)} />
      </div>
    )
  }
}
