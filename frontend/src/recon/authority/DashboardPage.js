import React from "react"

import NestedContainer from "core/NestedContainer"
import { SideBar } from "recon/common/SideBar"
import { Header } from "recon/common/Header"
import { generateRouteMethods } from "recon/utils"

export function DashboardPage(props) {
  const routeMethods = generateRouteMethods(props)

  return (
    <div className="vh-100 flex flex-column">
      <Header />
      <NestedContainer className="ch flex-auto" innerClassName="bg-black-10">
        <SideBar selectedKey="routeHome" routeMethods={routeMethods} />
      </NestedContainer>
    </div>
  )
}
