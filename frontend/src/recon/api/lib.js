export default function(config) {
  let { baseUrl } = config

  const addAuth = options => {
    let token = localStorage.getItem("token")

    if (token) {
      options.headers["Authorization"] = "Bearer " + token
    }

    return options
  }

  const ajax = (method, url, options, headers) => {
    url = baseUrl + url

    options.method = method

    options.headers = headers

    options = addAuth(options)

    console.info(method, url)

    return new Promise((resolve, reject) => {
      fetch(url, options)
        .then(response => response.json())
        .then(result => {
          if (result.error) reject(result.error)
          else resolve(result)
        })
        .catch(e => {
          console.error("ERROR", e)
          reject(e)
        })
    })
  }

  const json = (method, url, options) => {
    return ajax(method, url, options, {
      Accept: "application/json",
      "Content-Type": "application/json; charset=utf-8",
    })
  }

  const multipart = (url, options) => {
    return ajax("post", url, options, {
      Accept: "application/json",
    })
  }

  return {
    GET: (url, options) => {
      if (!options) options = {}
      return json("get", url, options).then(r => {
        console.log(r)
        return r
      })
      //.then(r => {console.log(JSON.stringify(r, null, 2)); return r})
    },

    POST: (url, data, options) => {
      if (!options) options = {}
      options.body = JSON.stringify(data)
      return json("post", url, options).then(r => {
        console.log(r)
        return r
      })
      //.then(r => {console.log(JSON.stringify(r, null, 2)); return r})
    },

    POST_MULTIPART: (url, data, options) => {
      if (!options) options = {}
      options.body = data
      return multipart(url, options).then(r => {
        console.log(r)
        return r
      })
      //.then(r => {console.log(JSON.stringify(r, null, 2)); return r})
    },

    PUT: (url, data, options) => {
      if (!options) options = {}
      options.body = JSON.stringify(data)
      return json("put", url, options).then(r => {
        console.log(r)
        return r
      })
      //.then(r => {console.log(JSON.stringify(r, null, 2)); return r})
    },

    DELETE: (url, options) => {
      if (!options) options = {}
      return json("delete", url, options).then(r => {
        console.log(r)
        return r
      })
      //.then(r => {console.log(JSON.stringify(r, null, 2)); return r})
    },
  }
}
