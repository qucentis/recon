import Eth from "ethjs"
import sigUtil from "eth-sig-util"
import BN from "bn.js"

export function toHex(s) {
  var hex = ""
  for (var i = 0; i < s.length; i++) {
    hex += "" + s.charCodeAt(i).toString(16)
  }
  return `0x${hex}`
}

export function signMessage(msgParams, from, callback) {
  window.web3.currentProvider.sendAsync(
    {
      method: "eth_signTypedData",
      params: [msgParams, from],
      from: from,
    },
    function(err, result) {
      if (err) return console.error(err)
      if (result.error) {
        return console.error(result.error.message)
      }

      const recovered = sigUtil.recoverTypedSignature({
        data: msgParams,
        sig: result.result,
      })

      if (recovered === from) {
        callback(null, recovered)
      } else {
        callback({ err: "failed to verify signer", result }, null)
      }
    }
  )
}

export function authenticate() {
  let token = localStorage.getItem("token")

  if (token) {
    // todo: check token validity
    return
  }

  if (!window.web3) {
    return alert("no web3 provider")
  }

  const msgParams = [
    {
      type: "string",
      name: "Message",
      value: "open sesame",
    },
  ]

  window.web3.eth.getAccounts(function(err, accounts) {
    if (!accounts) return

    let from = accounts[0]

    window.web3.currentProvider.sendAsync(
      {
        method: "eth_signTypedData",
        params: [msgParams, from],
        from: from,
      },
      function(err, result) {
        if (err) return console.error(err)
        if (result.error) {
          return console.error(result.error.message)
        }

        window.api
          .authenticate({
            sig: result.result,
            owner: from,
          })
          .then(result => {
            if (result.error) {
              console.error(result.error)
            } else {
              localStorage.setItem("token", result.token)
              localStorage.setItem("user", result.user)
            }
          })
          .catch(err => {
            console.error(err)
          })
      }
    )
  })
}

export function waitForTransaction(txhash) {
  let eth = new Eth(window.web3.currentProvider)
  let timer = null

  return new Promise((resolve, reject) => {
    const check = () => {
      eth.getTransactionReceipt(txhash).then(result => {
        if (result) {
          clearInterval(timer)
          resolve(result)
        }
      })
    }

    timer = setInterval(check, 1000)
  })
}

export function deployUserFactory(source) {
  let input = { "contract.sol": source }
  let eth = new Eth(window.web3.currentProvider)
  let from = localStorage.getItem("user")
  let data = null
  let abi = null

  return new Promise((resolve, reject) => {
    window.api
      .compile(input)
      .then(output => {
        data = output.contracts["contract.sol:UserContractFactory"]

        if (!data) {
          console.error(data.errors)
          return
        }

        abi = JSON.parse(data.interface)

        return eth.estimateGas({
          from,
          gas: "1000000",
          data: data.bytecode,
        })
      })
      .then(gas => {
        let Contract = eth.contract(abi, data.bytecode, {
          from,
          gas: gas.mul(new BN("2")).toString(),
        })

        return Contract.new(from)
      })
      .then(txhash => {
        return waitForTransaction(txhash)
      })
      .then(transaction => {
        console.log(transaction)
        resolve({ transaction, data })
      })
      .catch(err => {
        console.error(err)
        reject(err)
      })
  })
}

export function deployConsumerContract(factoryAddress, pastUsage) {
  let eth = new Eth(window.web3.currentProvider)
  let from = localStorage.getItem("user")
  let factory = null
  let address = null

  return new Promise((resolve, reject) => {
    window.api
      .deployedContract(factoryAddress)
      .then(contract => {
        let abi = JSON.parse(contract.data.interface)

        factory = eth
          .contract(abi, contract.data.bytecode, {
            from,
            gas: 1000000,
          })
          .at(factoryAddress)

        return factory.deploy(from, pastUsage)
      })
      .then(txhash => {
        return waitForTransaction(txhash)
      })
      .then(txn => {
        return factory.getContractForUser(from)
      })
      .then(result => {
        address = result[0]
        resolve(address)
      })
      .catch(err => {
        console.error(err)
        reject(err)
      })
  })
}
