import { testDeployUserFactory, testDeployConsumerContract } from "./test"

export function test() {
  let from = localStorage.getItem("user")

  if (
    from === "0x91304b2599dc9813338444e9ec3c28bd84c80fb2" ||
    from === "0x4bf18de3e3c0ac17ff05595d16d320b40350dad6"
  )
    testDeployUserFactory()
  else testDeployConsumerContract()
}
