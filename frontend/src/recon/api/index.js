import APIBase from "./lib"

const api = APIBase({
  baseUrl: "/api",
})

export default {
  get: url => {
    return api
      .GET(url)
      .then(result => console.log(result))
      .catch(err => console.error("ERROR", err))
  },

  post: (url, data) => {
    return api
      .POST(url, data)
      .then(result => console.log(result))
      .catch(err => console.error("ERROR", err))
  },

  put: (url, data) => {
    return api
      .PUT(url, data)
      .then(result => console.log(result))
      .catch(err => console.error("ERROR", err))
  },

  delete: url => {
    return api
      .DELETE(url)
      .then(result => console.log(result))
      .catch(err => console.error("ERROR", err))
  },

  authenticate: data => {
    return api.POST("/authenticate", data)
  },

  logout: () => {
    localStorage.removeItem("token")
    localStorage.removeItem("user")
  },

  testProtected: () => {
    return api.GET("/protected")
  },

  templates: () => {
    return api.GET("/templates")
  },

  compile: input => {
    return api.POST("/compile", { input })
  },

  addContract: data => {
    data.timestamp = Date.now()
    return api.POST("/add-contract", data)
  },

  deployedContracts: () => {
    return api.GET("/deployed-contracts")
  },

  deployedContract: address => {
    return api.GET(`/deployed-contract/${address}`)
  },
}
