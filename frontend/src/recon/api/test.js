import Eth from "ethjs"

import {
  signMessage,
  deployUserFactory,
  deployConsumerContract,
} from "./functions"

export function testGetBalance() {
  if (window.web3) {
    let eth = new Eth(window.web3.currentProvider)

    eth
      .accounts()
      .then(accounts => {
        return eth.getBalance(accounts[0])
      })
      .then(balance => {
        console.log(balance.toString())
      })
  }
}

export function testSignMessage() {
  if (window.web3) {
    const msgParams = [
      {
        type: "string",
        name: "Message",
        value: "Hi, Alice!",
      },
    ]

    window.web3.eth.getAccounts(function(err, accounts) {
      if (!accounts) return

      signMessage(msgParams, accounts[0])
    })
  }
}

export function testDeployUserFactory(callback) {
  let contract = null

  window.api
    .templates()
    .then(templates => {
      contract = templates[0]
      return deployUserFactory(contract.source, contract.contractName)
    })
    .then(({ transaction, data }) => {
      if (callback) callback(transaction.contractAddress)

      return window.api.addContract({
        address: transaction.contractAddress,
        name: contract.name,
        data,
      })
    })
    .then(result => {
      console.log(result)
    })
}

export function testDeployConsumerContract(callback) {
  window.api
    .deployedContracts()
    .then(contracts => {
      return contracts[contracts.length - 1]
    })
    .then(parent => {
      return deployConsumerContract(parent.address, 2000)
    })
    .then(address => {
      if (callback) callback(address)
      console.log(address)
    })
    .catch(err => {
      console.error(err)
    })
}
