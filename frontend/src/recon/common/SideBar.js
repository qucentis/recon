import React from "react"
import { Menu, Icon } from "antd"

const SubMenu = Menu.SubMenu

const menuRouteMethodGenerator = routeMethods => ({ key }) => {
  const noop = () => false
  const routeMethod = routeMethods[key] || noop
  routeMethod()
}

export function SideBar(props) {
  const { selectedKey, routeMethods } = props

  return (
    <div
      style={{ width: 180, minWidth: 180, height: "100%" }}
      className="theme-bg-white"
    >
      <Menu
        defaultSelectedKeys={[selectedKey]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        onClick={menuRouteMethodGenerator(routeMethods)}
      >
        <Menu.Item className="theme-whites" key="routeHome">
          <Icon type="pie-chart" />
          <span>Dashboard</span>
        </Menu.Item>
        <SubMenu
          key="sub1"
          title={
            <span>
              <Icon type="red-envelope" />
              <span>Contracts</span>
            </span>
          }
        >
          <Menu.Item key="routeToContracts">
            <Icon type="bars" />
            <span>List</span>
          </Menu.Item>
          <Menu.Item key="routeToContractCreate">
            <Icon type="plus" />
            <span>New</span>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </div>
  )
}
