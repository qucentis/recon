import React from "react"

export function NotFound({ ...props }) {
  return <div className="pa4 sans-serif f2">Not Found!</div>
}
