import React from "react"

import { css } from "styled-components"
import NestedContainer from "core/NestedContainer"
import { Logo } from "recon/common/Logo"

const minHeight = css`
  min-height: 4rem;
`

export function Header(argument) {
  return (
    <NestedContainer
      css={minHeight}
      className="ch theme-bg-gray"
      innerClassName="ph3 cv"
    >
      <div className="cv w-100">
        <div style={{ width: 256 }} className="">
          <Logo scale={0.25} />
        </div>
      </div>
    </NestedContainer>
  )
}
