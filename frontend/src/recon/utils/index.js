export function generateRouteMethods(props) {
  const { push, goBack } = props.history

  return {
    routeToContracts: () => push("/authority/contracts/"),
    routeToContractCreate: () => push("/authority/contract-templates/1/create"),
    routeBack: () => goBack(),
    routeHome: () => push("/"),
  }
}
