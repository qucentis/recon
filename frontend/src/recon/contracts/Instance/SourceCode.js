import React from "react"
import SyntaxHighlighter from "react-syntax-highlighter/prism"
import { atomDark } from "react-syntax-highlighter/styles/prism"

import { cardDimensions } from "config"

const contentHeight =
  cardDimensions.containerHeight - cardDimensions.tabbarHeight

export function SourceCode({ contractData }) {
  const source = contractData.source

  return (
    <div
      style={{ height: `${contentHeight}rem` }}
      className="bg-black pre code recon-contract-editor"
    >
      <SyntaxHighlighter showLineNumbers language="javascript" style={atomDark}>
        {source}
      </SyntaxHighlighter>
    </div>
  )
}
