import React from "react"
import styled from "styled-components"
import ReactMarkdown from "react-markdown"

export function Description({ contractData }) {
  const source = contractData.text

  return (
    <DescriptionText>
      <div className="help-text" style={{ height: 600 }}>
        <ReactMarkdown className="sans-serif f6" source={source} />
      </div>
    </DescriptionText>
  )
}

export const DescriptionText = styled.div.attrs({
  className: "pa3 overflow-scroll f6 lh-solid help-text",
})``
