import styled from "styled-components"

export const Cover = styled.div.attrs({
  className: "bb b--black-30",
})`
  box-shadow: 0 0.125rem 0.125rem 0 rgba(0, 0, 0, 0.3);
  height: 8rem;
  background: url(${p => p.coverImage});
  background-size: cover;
`
