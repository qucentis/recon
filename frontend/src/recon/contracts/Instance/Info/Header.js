import React from "react"
import styled from "styled-components"

export function Header({ name, displayPicture, address, percentageReduction }) {
  return (
    <div className="ml3 flex relative justify-between">
      <div className="flex relative">
        <DisplayPicture src={displayPicture} />
        <div className="flex flex-column" style={{ marginLeft: "7rem" }}>
          <Title>{name}</Title>
          <SubTitle>{address}</SubTitle>
        </div>
      </div>
      <div className="theme-bg-gray theme-green tracked br2 mr3 mt3 ca code p w2 h2">
        {percentageReduction}%
      </div>
    </div>
  )
}

const DisplayPicture = styled.img.attrs({
  className: "br3 absolute popout1",
  style: { top: "-3rem" },
  width: 96,
  height: 96,
})``

const Title = styled.div.attrs({
  className: "f4 fw5 pt1",
})``

const SubTitle = styled.div.attrs({
  className: "code f7 fw2",
})``
