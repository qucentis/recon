import { Component } from "react"

import { proposal } from "recon/data/contract"

export class DataProvider extends Component {
  render() {
    const render = this.props.render
    const contractData = proposal

    return render({ contractData })
  }
}
