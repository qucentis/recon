import React from "react"
import styled from "styled-components"
import { lighten } from "polished"
import { Icon } from "antd"

import { cardDimensions, theme } from "config"

export function Tabbar({
  tabIndex,
  setIndex,
  isInvoking,
  isMobile,
  isProposal,
}) {
  return (
    <div className="flex">
      <TabButton selected={0 === tabIndex} onClick={() => setIndex(0)}>
        <TabContent isMobile={isMobile} iconType="info-circle-o" text="Info" />
      </TabButton>
      <TabButton selected={1 === tabIndex} onClick={() => setIndex(1)}>
        <TabContent isMobile={isMobile} iconType="file-text" text="Terms" />
      </TabButton>
      <TabButton selected={2 === tabIndex} onClick={() => setIndex(2)}>
        <TabContent isMobile={isMobile} iconType="code-o" text="Source Code" />
      </TabButton>
      <TabButton selected={4 === tabIndex} onClick={() => setIndex(4)}>
        <TabContent isMobile={isMobile} iconType="qrcode" text="QR Code" />
      </TabButton>
      {isProposal && (
        <TabButton selected={5 === tabIndex} onClick={() => setIndex(5)}>
          <TabContent
            isMobile={isMobile}
            iconType={isInvoking ? "loading" : "edit"}
            text={isInvoking ? "Invoking" : "Invoke"}
          />
        </TabButton>
      )}
    </div>
  )
}

const TabContent = ({ iconType, text, isMobile }) => (
  <div className="flex flex-column pa2">
    <Icon style={{ fontSize: "1.5rem" }} type={iconType} />
    {!isMobile && (
      <div className="f7 mt2" style={{ color: theme.white }}>
        {text}
      </div>
    )}
  </div>
)

const TabButton = styled.div.attrs({
  className: "w-100 br f4 b--black-10 pointer ca",
})`
  height: ${cardDimensions.tabbarHeight}rem;
  color: ${p =>
    p.selected ? lighten(0.0, theme.green) : lighten(0.1, theme.white)};
  background: ${p =>
    p.selected ? lighten(0.0, theme.gray) : lighten(0.15, theme.gray)};
`
