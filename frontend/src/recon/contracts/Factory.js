import React from "react"

import { Proposal } from "./Proposal"
import { Instance } from "./Instance"

const typeMap = {
  proposal: props => <Proposal {...props} />,
  instance: props => <Instance {...props} />,
}

export const Factory = ({ data }) =>
  typeMap[data.type]({
    contractData: {
      ...data.payload,
      timestamp: data.timestamp,
      address: data.address,
    },
  })
