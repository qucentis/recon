import React from "react"
import { Index } from "react-powerplug"

import { Tabbar } from "../Tabbar"
import { Info } from "./Info"
import { SourceCode } from "./SourceCode"
import { Description } from "./Description"
import { QRCodeDisplay } from "./QRCodeDisplay"
import { Tabs, Tab, Container } from "./StyledComponents"

export function InstanceMobile({ contractData }) {
  const url = `http://project-recon.com/contracts/${contractData.address}`

  return (
    <Container>
      <Index
        initial={0}
        render={({ index, setIndex }) => (
          <div className="w-100 h-100 flex flex-column">
            <Tabs>
              <Tab show={0 === index}>
                <Info contractData={contractData} />
              </Tab>
              <Tab show={1 === index}>
                <Description contractData={contractData} />
              </Tab>
              <Tab show={2 === index}>
                <SourceCode contractData={contractData} />
              </Tab>
              <Tab show={4 === index}>
                <QRCodeDisplay url={url} />
              </Tab>
            </Tabs>
            <Tabbar isMobile tabIndex={index} setIndex={setIndex} />
          </div>
        )}
      />
    </Container>
  )
}
