import React from "react"
import strftime from "strftime"

import { Header } from "./Header"
import { Cover } from "./Cover"
import { Content } from "./Content"

export function Info({ contractData }) {
  const date = strftime("%b %e %Y", new Date(contractData.timestamp * 1000))
  const address = contractData.address
  const url = `https://rinkeby.etherscan.io/address/${address}`

  return (
    <div className="flex flex-column h-100">
      <Cover {...contractData} />
      <Header {...contractData} />
      <div className="theme-bg-white w-100 h2 mt3 cv ph3 code f7">
        Recon contract deployed on {date} to&nbsp;<a className="b" href={url}>
          Rinkeby
        </a>
      </div>
      <Content {...contractData} />
    </div>
  )
}
