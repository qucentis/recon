import React from "react"
import { Steps, Icon } from "antd"

export function Content({ issuer, status, duration, contractParty, address }) {
  const Step = Steps.Step

  return (
    <div className="flex w-100 bg-black-10s mt1">
      <div className="ph3 w-100">
        <Steps current={6}>
          <Step
            title="Issuer"
            description={<Title>{issuer.name}</Title>}
            icon={<Icon type="solution" />}
          />
          <Step
            title="Party"
            description={<Title>{contractParty.accountNumber}</Title>}
            icon={<Icon type="user" />}
          />
          <Step
            title="Period"
            description={<Title>{duration}</Title>}
            icon={<Icon type="clock-circle-o" />}
          />
          <Step
            title="Status"
            description={
              <Title>
                {status.isResolved
                  ? status.isSuccess ? "Success" : "Failure"
                  : "Pending"}
              </Title>
            }
            icon={
              <Icon
                type={
                  status.isResolved
                    ? status.isSuccess ? "check-circle-o" : "close-circle-o"
                    : "play-circle-o"
                }
              />
            }
          />
        </Steps>
      </div>
    </div>
  )
}

const Title = ({ children }) => <div className="b">{children}</div>
