import React from "react"
import styled from "styled-components"

import { cardDimensions } from "config"

const tabsHeight = cardDimensions.containerHeight - cardDimensions.tabbarHeight

export const Tabs = styled.div.attrs({
  className: "w-100 bg-light-green",
})`
  min-height: ${tabsHeight}rem;
`

export function Tab({ show, children, scrollable }) {
  if (!show) {
    return null
  }

  return <TabContainer scrollable={scrollable}>{children}</TabContainer>
}

const TabContainer = styled.div.attrs({
  className: "w-100 bg-white ",
})`
  height: ${tabsHeight}rem;
  ${p => p.scrollable && "overflow-y: scroll"};
`

export const Container = styled.div.attrs({
  className: "bg-white flex br3 overflow-hidden popout1",
})`
  width: 40rem;
  height: ${cardDimensions.containerHeight}rem;
`

export const Cover = styled.div.attrs({
  className: "w3 h3 bg-light-yellow",
})``

export const Content = styled.div.attrs({
  className: "w3 h3 bg-red",
})``
