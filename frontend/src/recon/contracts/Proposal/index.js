import React from "react"
import { Index } from "react-powerplug"
import { withRouter } from "react-router-dom"

import { Tabbar } from "../Tabbar"
import { Info } from "./Info"
import { SourceCode } from "./SourceCode"
import { Description } from "./Description"
import { QRCodeDisplay } from "./QRCodeDisplay"
import { Invoker } from "../Invoker"
import { Tabs, Tab, Container } from "./StyledComponents"

import { testDeployConsumerContract } from "recon/api/test"

class Proposal extends React.Component {
  deployContract = () => {
    testDeployConsumerContract(address => {
      this.props.history.push(`/contracts/${address}`)
    })
  }

  render() {
    const { contractData } = this.props
    const url = `http://project-recon.com/contracts/${contractData.address}`

    return (
      <Container>
        <Index
          initial={0}
          render={({ index, setIndex }) => (
            <div className="w-100">
              <Tabs>
                <Tab show={0 === index}>
                  <Info contractData={contractData} />
                </Tab>
                <Tab scrollable show={1 === index}>
                  <Description contractData={contractData} />
                </Tab>
                <Tab show={2 === index}>
                  <SourceCode contractData={contractData} />
                </Tab>
                <Tab show={4 === index}>
                  <QRCodeDisplay url={url} />
                </Tab>
                <Tab show={5 === index}>
                  <Invoker contractData={contractData} url={url} />
                </Tab>
              </Tabs>
              <Tabbar isProposal tabIndex={index} setIndex={setIndex} />
            </div>
          )}
        />
      </Container>
    )
  }
}

Proposal = withRouter(Proposal)
export { Proposal }
