import React from "react"
import { Steps, Icon } from "antd"

export function Content({ issuer, status, duration, contractParty, address }) {
  const Step = Steps.Step

  return (
    <div className="flex w-100 bg-black-10s mt3">
      <div className="ph3 w-100">
        <Steps current={6}>
          <Step
            title="Issuer"
            description={<Title>{issuer.name}</Title>}
            icon={<Icon type="solution" />}
          />
          <Step
            title="Period"
            description={<Title>{duration}</Title>}
            icon={<Icon type="clock-circle-o" />}
          />
        </Steps>
      </div>
    </div>
  )
}

const Title = ({ children }) => <div className="b">{children}</div>
