import React from "react"

import { ProposalMobile } from "./ProposalMobile"
import { InstanceMobile } from "./InstanceMobile"

const typeMap = {
  proposal: props => <ProposalMobile {...props} />,
  instance: props => <InstanceMobile {...props} />,
}

export const FactoryMobile = ({ data }) =>
  typeMap[data.type]({
    contractData: {
      ...data.payload,
      timestamp: data.timestamp,
      address: data.address,
    },
  })
