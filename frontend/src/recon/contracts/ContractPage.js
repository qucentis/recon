import React from "react"
import { css } from "styled-components"
import { lighten } from "polished"
import Responsive from "react-responsive"

import NestedContainer from "core/NestedContainer"
import { Header } from "recon/common/Header"
import { Factory } from "./Factory"
import { FactoryMobile } from "./FactoryMobile"
import { DataProvider } from "./DataProvider"

import { theme, dimensions } from "config"

const Desktop = props => <Responsive {...props} {...dimensions.desktop} />
const Mobile = props => <Responsive {...props} {...dimensions.mobile} />

export function ContractPage(props) {
  return (
    <DataProvider
      render={({ contractData }) => (
        <div className="vh-100 flex flex-column">
          <Header />
          <NestedContainer
            css={grid}
            className="ch flex-auto"
            innerClassName=""
          >
            <div className="h-100 ca">
              <Desktop>
                <Factory data={contractData} />
              </Desktop>
              <Mobile>
                <FactoryMobile data={contractData} />
              </Mobile>
            </div>
          </NestedContainer>
        </div>
      )}
    />
  )
}

const grid = css`
  background: ${lighten(0.9, theme.white)};
  background: url(/grid.svg);
`
