import React from "react"
import styled from "styled-components"

export const Tabs = styled.div.attrs({
  className: "w-100 h-100 flex-auto overflow-scroll",
})``

export function Tab({ show, children }) {
  if (!show) {
    return null
  }

  return <TabContainer>{children}</TabContainer>
}

const TabContainer = styled.div.attrs({
  className: "w-100 h-100",
})``

export const Container = styled.div.attrs({
  className: "flex overflow-hidden h-100",
})`
  width: 100%;
`

export const Cover = styled.div.attrs({
  className: "w3 h3 theme-bg-gray",
})``

export const Content = styled.div.attrs({
  className: "w3 h3",
})``
