import React from "react"
import QRCode from "qrcode.react"
import { lighten } from "polished"

import { theme } from "config"

const lightTheme = lighten(0.75, theme.gray)

export function QRCodeDisplay({ url }) {
  return (
    <div className="ca h-100" style={{ background: lightTheme }}>
      <div className="flex cv flex-column">
        <QRCode
          value={url}
          size={256}
          bgColor={lightTheme}
          fgColor={theme.gray}
          level={"L"}
        />
        <a
          href={url}
          className="tc mt4 f7 link hover-white theme-bg-gray theme-white pv1 ph2 br2"
        >
          Recon Contract Link
        </a>
      </div>
    </div>
  )
}
