import React from "react"
import styled from "styled-components"
import ReactMarkdown from "react-markdown"

export function Description({ contractData }) {
  const source = contractData.text

  return (
    <DescriptionText>
      <div className="help-text" style={{ width: 253 }}>
        <ReactMarkdown className="sans-serif f6" source={source} />
      </div>
    </DescriptionText>
  )
}

export const DescriptionText = styled.div.attrs({
  className: "pa3 f6 lh-solid help-text",
})`
  overflow: hidden;

  &:hover {
    overflow-y: scroll;
  }
`
