import React from "react"
import { Input, Button } from "antd"

export class Invoker extends React.Component {
  state = {
    step: 0,
    accountNumber: "2569841",
    previousUsage: 1400,
    target: 1386,
    isInvoking: false,
  }

  nextStep = () => {
    this.setState(s => ({ step: 1 }))
  }

  invoke = () => this.setState(s => ({ isInvoking: true }))

  render() {
    const step = this.state.step
    const { contractData } = this.props
    const state = this.state

    return (
      <div className="bg-white pa4 h-100">
        <div className="f3 tc b">Invoke Energy Saving Contract</div>
        {step === 0 && <GetUsageData nextStep={this.nextStep} />}
        {step === 1 && (
          <ConfirmSection
            isInvoking={this.state.isInvoking}
            invoke={this.invoke}
            {...contractData}
            {...state}
          />
        )}
      </div>
    )
  }
}

const ConfirmSection = ({
  isInvoking,
  issuer,
  percentageReduction,
  duration,
  accountNumber,
  previousUsage,
  target,
  invoke,
}) => (
  <div className="">
    <div className="mv3 ph3">
      Clicking <b>Agree & Invoke</b> will mean that you are engaging into a
      contract with <b>{issuer.name}</b> that the energy usage from the{" "}
      <b>DEWA Account Number: {accountNumber} </b> will be reduced by{" "}
      <b>{percentageReduction}</b>% for the billing cycle <b>{duration}</b> as
      per the <b>Terms</b> mentioned. Previous usage for the account is{" "}
      <b>{previousUsage}</b> & the target usage is <b>{target}</b>.
    </div>
    <div className="ph3">
      <Button
        size="large"
        style={{ width: "100%", marginTop: "2rem" }}
        type="primary"
        onClick={invoke}
      >
        {isInvoking ? "Invoking" : "Accept & Invoke"}
      </Button>
    </div>
  </div>
)

const GetUsageData = ({ nextStep }) => (
  <div>
    <div className="mv3 ph3">
      Please enter your <b>DEWA Account Number</b> to get your previous usage
      data & see the saving you need to commit to for the contract.
    </div>
    <div className="ph3">
      <Input
        placeholder="xxxxxxx"
        style={{
          fontSize: "2rem",
          padding: "2rem 2rem",
          textAlign: "center",
        }}
      />
      <Button
        size="large"
        style={{ width: "100%", marginTop: "2rem" }}
        type="primary"
        onClick={nextStep}
      >
        Get Usage Data
      </Button>
    </div>
  </div>
)
