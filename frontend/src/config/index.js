/*
  All dimensions in rem
*/

export const contentWidth = 90

export const dimensions = {
  mobile: {
    maxWidth: "51rem",
  },
  desktop: {
    minWidth: "51rem",
  },
}

export const cardDimensions = {
  containerHeight: 28,
  tabbarHeight: 5,
}

export const theme = {
  gray: "#232C3D",
  green: "#4AFF5B",
  white: "#F3F4F5",
}
