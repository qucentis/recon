import React from "react"
import ReactDOM from "react-dom"
import App from "recon/App"
import registerServiceWorker from "./registerServiceWorker"

import "tachyons"
import "./styles/fonts/default.css"
import "./styles/theme.css"
import "./styles/hacks.css"
import "./styles/tachyons-extended.css"

ReactDOM.render(<App />, document.getElementById("root"))
registerServiceWorker()
