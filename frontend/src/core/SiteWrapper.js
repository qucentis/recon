import styled from "styled-components"

const SiteWrapper = styled.div.attrs({
  className: "flex flex-column",
})`
  min-height: 100vh;
`

export default SiteWrapper
