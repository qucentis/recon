# Recon

Recon is a blockchain based protocol for resource conservation. It influences consumer behaviour by providing financial incentivisation (which is proposed as an internationally recognized carbon credits scheme).

## Context

> Energy saved is, energy generated.

The insatiable growth of user needs requires an ever growing energy generation capacity. Most clean energy generation initiatives focus on renewable energy generation. But any amount of energy thats conserved is equivalent to generation. So we are building a solution to trade saved energy. Using blockchain technology we are building a smart contract system that can be utilized to influence consumer behaviour & facilitate carbon market.
