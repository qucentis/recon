# Research Sources

## Cop21

- [Unfinished Agenda](https://www.brookings.edu/blog/planetpolicy/2015/12/22/the-unfinished-agenda-of-the-paris-climate-talks-finance-to-the-global-south/)
- [BY the numbers - NPR](https://www.npr.org/sections/thetwo-way/2015/12/12/459502597/2-degrees-100-billion-the-world-climate-agreement-by-the-numbers)
- [Financial Implications of COP21](https://www.carbontracker.org/tools-and-insights/panel-2-what-are-the-financial-implications-of-cop21/)

## UAE Specific

- [INDC UAE Submission](http://www4.unfccc.int/Submissions/INDC/Published%20Documents/United%20Arab%20Emirates/1/UAE%20INDC%20-%2022%20October.pdf)
